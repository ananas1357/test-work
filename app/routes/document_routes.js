let requestQueue = Promise.resolve();

async function addDocument(col, res, data) {
    const ttl = data.ttl;
    const createdAt = data.createdAt;
    delete data.ttl;
    delete data.createdAt;
    const oldDocument = await col.findOne(data);
    if (!oldDocument) {
        data.expireAt = new Date(Date.now() + ttl * 1000);
        data.createdAt = createdAt;
        const newDocument = await col.insert(data);
        if (!newDocument) {
            console.error(newDocument);
            res.send({ error: 'Error insert new document' });
            return { error: 'Error insert new document' };
        } else {
            console.log(newDocument.ops[0]);
            res.send({ 'result': true });
            return { success: 'Success added document' };
        }
    } else {
        res.send({ 'result': false });
        console.log(oldDocument);
        return { error: 'This document is already created' };
    }
}

module.exports = (app, col) => {
    // Create
    app.post('/documents', (req, res) => {
        // Здесь оставил такое условие, чтобы документы с ttl = 0 не добавлять совсем
        // Для добавления любых ttl в том числе равных 0 можно использовать условие
        // if (req.body.hasOwnProperty('ttl'))
        if (req.body.ttl) {
            const data = {};
            Object.assign(data, req.body);
            data.createdAt = new Date();
            requestQueue = requestQueue
                .then(() => addDocument(col, res, data).then((info) => {
                        console.log(info);
                    }).catch((err) => {
                        console.error(err);
                        res.send({ error: 'Error with add document' });
                    })
                );
        } else {
            console.error({ 'error': 'Document haven\'t ttl!' });
            res.send({ 'error': 'Document haven\'t ttl!' })
        }
    });
};