const documentRoutes = require('./document_routes');

module.exports = (app, col) => {
    documentRoutes(app, col);
};