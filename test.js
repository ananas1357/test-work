const request = require('request');

for (let i = 0; i < 20; ++i) {
    request.post(
        'http://127.0.0.1:8000/documents',
        { json: { key: 'value', name: 'test', ttl: 10 } },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body);
            } else {
                console.log(error);
                console.log(response.statusCode);
            }
        }
    );
    request.post(
        'http://127.0.0.1:8000/documents',
        { json: { key: 'value', name: `test${i}`, ttl: i } },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body)
            } else {
                console.log(error);
                console.log(response.statusCode);
            }
        }
    );
}

setTimeout(() => {
    request.post(
        'http://127.0.0.1:8000/documents',
        { json: { key: 'value', name: `test4sec`, ttl: 4 } },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body);
            } else {
                console.log(error);
                console.log(response.statusCode);
            }
        }
    );
}, 4000);