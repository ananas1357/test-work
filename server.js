const express = require('express');
const bodyParser = require('body-parser');
const { MongoClient } = require('mongodb');
const db = require('./config/db');
const Routes = require('./app/routes');

const app = express();
const port = 8000;
app.use(bodyParser.json());
MongoClient.connect(db.url, (err, database) => {
    if (err) return console.error(err);

    const collection = database.collection('documents');
    collection.createIndex( { "expireAt": 1 }, { expireAfterSeconds: 0 } );
    app.get('/', (req, res) => {
        collection.find({}).toArray(function(err, docs){
            if (err) {
                res.json({'error': 'Error with find all documents'});
                return console.error(err);
            }
            res.json(docs);
        });
    });
    Routes(app, collection);
    app.listen(port, () => {
        console.log(`Server start on ${port}`);
    });

    database.on('close', function () {
        console.error('Error connection close');
    });
});
